# TrackerGenerator - Torrent Tracker Generator

🇹🇷 Basit bir torrent tracker oluşturucusu.

🇬🇧 A simple torrent tracker generator.

## Lisans

![](https://www.gnu.org/graphics/gplv3-127x51.png)

You can use, study share and improve it at your will. Specifically you can redistribute and/or modify it under the terms of the [GNU General Public License](https://www.gnu.org/licenses/gpl-3.0.html) as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
